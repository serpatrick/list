export default function (index: number, max: number) {
  if (index < 0) {
    return max + index;
  }

  return index;
}
